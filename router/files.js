const express = require('express');
const fs = require('fs');

const router = express.Router();
const fsPromises = fs.promises;

const getExtension = (filename) => {
	return filename.split('.').pop();
};

const isValidFile = (filename) => {
	const extension = getExtension(filename);
	const supportedExtensions = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];
	return supportedExtensions.includes(extension);
};

const getFile = async (req, res) => {
	const { filename } = req.params;
	if (!isValidFile(filename)) {
		return res.status(406).json({
			message:
				'Please specify "filename" parameter with: log, txt, json, yaml, xml, js file extensions',
		});
	}

	try {
		const content = await fsPromises.readFile(`./data/${filename}`, 'utf8');
		const fileStat = await fsPromises.stat(`./data/${filename}`);
		return res.status(200).json({
			message: 'Success',
			filename,
			content,
			extension: getExtension(filename),
			uploadedDate: fileStat.mtime,
		});
	} catch (error) {
		return res
			.status(500)
			.json({ message: `Server error: ${error.message}` });
	}
};

const getFiles = async (req, res) => {
	try {
		const files = await fsPromises.readdir('./data');
		res.status(200).json({ message: 'Success', files });
	} catch (error) {
		res.status(500).json({ message: `Server error: ${error.message}` });
	}
};

const createFile = async (req, res) => {
	const { filename, content } = req.body;
	if (!isValidFile(filename)) {
		return res.status(406).json({
			message:
				'Please specify "filename" parameter with: log, txt, json, yaml, xml, js file extensions',
		});
	}

	if (!content) {
		return res
			.status(400)
			.json({ message: 'Please specify "content" parameter' });
	}

	try {
		await fsPromises.writeFile(`./data/${filename}`, content);
		return res.status(200).json({ message: 'File created successfully.' });
	} catch (error) {
		return res
			.status(500)
			.json({ message: `Server error: ${error.message}` });
	}
};

const updateFile = async (req, res) => {
	const { filename, content } = req.body;

	if (!isValidFile(filename)) {
		return res.status(406).json({
			message:
				'Please specify "filename" parameter with: log, txt, json, yaml, xml, js file extensions',
		});
	}

	try {
		await fsPromises.appendFile(`./data/${filename}`, content);
		return res.status(200).json({ message: 'File appended successfully.' });
	} catch (error) {
		return res
			.status(500)
			.json({ message: `Server error: ${error.message}` });
	}
};

const deleteFile = async (req, res) => {
	const { filename } = req.params;
	if (!isValidFile(filename)) {
		return res.status(406).json({
			message:
				'Please specify "filename" parameter with: log, txt, json, yaml, xml, js file extensions',
		});
	}

	try {
		await fsPromises.unlink(`./data/${filename}`);
		return res.status(200).json({ message: 'File deleted successfully.' });
	} catch (error) {
		return res
			.status(500)
			.json({ message: `Server error: ${error.message}` });
	}
};

router.post('/', createFile);
router.get('/:filename', getFile);
router.get('/', getFiles);
router.put('/', updateFile);
router.delete('/:filename', deleteFile);

module.exports = router;
