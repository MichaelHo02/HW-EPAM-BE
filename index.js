const express = require('express');
const morgan = require('morgan');
const files = require('./router/files.js');

const app = express();
app.use(express.json());
app.use(morgan('combined'));

app.use('/api/files', files);

app.listen(8080);
console.log('Successfully start server');
